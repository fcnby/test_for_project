# Тестовое задание для проекта Ибрагимов Кирилл

---
#### Задание 1
В папке Task_2 лежит Dockerfile с микросервисом котика.
Для запуска использовал следующую команду:
```
docker build -t cat .
docker run -p 8080:8080 cat
```
---
#### Задание 2
В папке Task_2 лежит Dockerfile с микросервисом собаки.
Для запуска использовал следующую команду:
```
docker build -t dog .
docker run -p 8080:8080 dog
```
---
#### Задание 3 

В папке Task_2 лежит Dockerfile с микросервисом ребенка.
Для запуска использовал следующую команду:
```
docker network create my_net
docker build -t child .
docker run --network my_net --name child_service -p 8080:8080 --env MS_DOG_HOST=dog --env MS_CAT_HOST=cat -d child
```
---
#### Задание 4 

Для запуска подобного окружения использую следующие команды:
```
docker network create my_net
docker build -t child .
docker run --network my_net --name child_service -p 8080:8080 --env MS_DOG_HOST=dog --env MS_CAT_HOST=cat -d child
docker run --network my_net --name dog --env -d dog
docker run --network my_net --name cat --env -d cat
```
---
#### Задание 5 
docker-compose.yaml - файл для запуска окружения из предыдущего задания.
