package main

import (
	"fmt"
	"io"
	"net/http"
)

func whatCatSay(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "Wof\n")
}

func whoiami(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "Dog")
}

func main() {
	http.HandleFunc("/", whatCatSay)
	http.HandleFunc("/whoami", whoiami)
	fmt.Printf("Try to start on port 8080")
	if err := http.ListenAndServe(":8080", nil); err != nil {
		fmt.Printf("An error: %s", err)
	}
}
